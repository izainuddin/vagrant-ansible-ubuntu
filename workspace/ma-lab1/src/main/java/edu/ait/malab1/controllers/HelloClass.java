package edu.ait.malab1.controllers;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloClass {
    @GetMapping("hello-msg")
    public String sayHello()
    {
        return "GET - hello \n";       
    }

    @PostMapping("post-something")
    public String postHello()
    {
        return "POST- Hello \n";       
    }

    @PutMapping("put-something")
    public String putHello()
    {
        return "PUT - Hello \n ";
    }

    @DeleteMapping("delete-something")
    public String deleteHello()
    {
        return "DELETE - Hello1 \n ";
    }

    
}