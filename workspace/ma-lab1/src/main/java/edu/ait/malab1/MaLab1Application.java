package edu.ait.malab1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaLab1Application {

	public static void main(String[] args) {
		SpringApplication.run(MaLab1Application.class, args);
	}

}
